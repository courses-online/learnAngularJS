angular.module("MyFirstApp",[])
	.controller("FirstController",["$scope","$http",function(s,h){
		s.posts=[];
		s.newPost={};
		h.get("http://jsonplaceholder.typicode.com/posts")
		.success(function(data){
			console.log(data);
			s.posts=data;
		})
		.error(function(err){

		});

		s.addPost=function(){
			h.post("http://jsonplaceholder.typicode.com/posts",{
				title: s.newPost.title,
				body: s.newPost.body,
				userId: 1
			})
			.success(function(data,status,headers,config){
				console.log(data)
				s.posts.push(data);
				s.newPost={};
			})
			.error(function(err,status,headers,config){
				console.log(err)

			});
		}
	}]);
