angular.module("MyFirstApp",[])
	.controller("FirstController",["$scope","$http",function(m,h){
		m.nombre = "Luis  Fernando Garcia";
		m.nuevoComentario={};
		m.comentarios =[
		{
			comentario: "Buen Tutorial",
			username: "Trossky"
		},
		{
			comentario: "Mal Tutorial",
			username: "otro_user"
		}

		] ;
		m.agregarComentario= function(){
			m.comentarios.push(m.nuevoComentario);
			m.nuevoComentario={};

		}
	}]);

//MVM