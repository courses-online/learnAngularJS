angular.module('FinalApp')
.controller("MainController",function($scope,$resource){

	Post=$resource("http://jsonplaceholder.typicode.com/posts/:id",{id:"@id"});
	User=$resource("http://jsonplaceholder.typicode.com/users/:id",{id:"@id"});

	$scope.posts=Post.query();
	//query() -> GET/posts   -> return-  Arreglo de Posts -isArray: true
	$scope.users=User.query();

	$scope.removePost=function(post){
		Post.delete({id: post.id},function(data){
			console.log(data);

		});
		$scope.posts=$scope.posts.filter(function(element) {
			return element.id !==post.id;
		});
	};
	
})
.controller("PostController",function($scope,$resource,$routeParams){
	Post=$resource("http://jsonplaceholder.typicode.com/posts/:id",{id:"@id"});
	//RES FUL tiene el metodo GET, y de esta forma 
	$scope.post=Post.get({id: $routeParams.id});
	
});