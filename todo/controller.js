angular.module("ToDoList",["LocalStorageModule"])
.controller("ToDoController", function($scope, localStorageService){

	if(localStorageService.get("angular-todolist")){
		$scope.todo=localStorageService.get("angular-todolist");

	}else{
		$scope.todo=[];	
	}
	
	/*
	 {
		descripcion: 'Terminar el Curso Angular'
		fecha: '03-03-15 2:00pm'
	 }
	*/
	//Para no repetir codigo
	$scope.$watchCollection("todo", function(newValue,oldValue){
		localStorageService.set("angular-todolist",$scope.todo);
	});
	
	$scope.addActv=function(){
		$scope.todo.push($scope.newActv);
		$scope.newActv={};
	}
});