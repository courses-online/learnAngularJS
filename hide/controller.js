angular.module("MyFirstApp",[])
	.controller("FirstController",["$scope","$http",function(s,h){
		s.posts=[];
		s.loading=true;
		h.get("http://jsonplaceholder.typicode.com/posts")
		.success(function(data){
			console.log(data);
			s.posts=data;
			s.loading=false;

		})
		.error(function(err){
			s.loading=false;
		});

	}]);
